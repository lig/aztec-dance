import 'package:flutter/foundation.dart';

class ParamsModel extends ChangeNotifier {
  FieldColor _fieldColor = FieldColor.monochrome;
  FieldColor get fieldColor => _fieldColor;
  set fieldColor(FieldColor? value) {
    if (value == null) return;
    _fieldColor = value;
    notifyListeners();
  }
}

enum FieldColor { checkered, monochrome }
