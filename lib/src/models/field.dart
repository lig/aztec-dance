import 'dart:math';

import 'package:flutter/foundation.dart';

typedef Board = Map<Point, Cell>;

class FieldModel extends ChangeNotifier {
  static Random rnd = Random();

  int _size = 0;
  int get size => _size;
  int get _radius => _size * 2 - 1;

  Board _board = {};
  Board get board => _board;

  Step _nextStep = Step.extend;
  Step get nextStep => _nextStep;

  void step() {
    Board boardUpdate;
    switch (_nextStep) {
      case Step.extend:
        _nextStep = Step.slide;
        boardUpdate = _stepExtend();
        break;
      case Step.slide:
        _nextStep = Step.fill;
        boardUpdate = _stepSlide();
        break;
      case Step.fill:
        _nextStep = Step.annihilate;
        boardUpdate = _stepFill();
        break;
      case Step.annihilate:
        _nextStep = Step.extend;
        boardUpdate = _stepAnnihilate();
        break;
    }
    _mutateBoard(boardUpdate.entries);
    notifyListeners();
  }

  Board _stepExtend() {
    _size++;
    return Map.fromEntries([
      for (var y = -_radius; y <= _radius; y += 2)
        for (var sign in [-1, 1])
          Cell(
            point: Point(
              x: sign * (_radius + 1 - y.abs()),
              y: y,
            ),
          )
    ].map((cell) => MapEntry(cell.point, cell)));
  }

  Board _stepSlide() {
    Board boardUpdate = {};

    for (var cellEntry in _board.entries) {
      Point point = cellEntry.key;
      Cell cell = cellEntry.value;

      if (cell.runtimeType == Cell) continue;

      cell = cell as DominoCell;
      Domino domino = cell.domino;

      Point nextPoint;
      switch (domino.orientation) {
        case Orientation.horizontal:
          switch (domino.direction) {
            case Direction.negative:
              nextPoint = Point(x: point.x, y: point.y - 2);
              break;
            case Direction.positive:
              nextPoint = Point(x: point.x, y: point.y + 2);
              break;
          }
          break;
        case Orientation.vertical:
          switch (domino.direction) {
            case Direction.negative:
              nextPoint = Point(x: point.x - 2, y: point.y);
              break;
            case Direction.positive:
              nextPoint = Point(x: point.x + 2, y: point.y);
              break;
          }
          break;
      }

      boardUpdate[nextPoint] = DominoCell(
        point: nextPoint,
        domino: Domino(
          point: nextPoint,
          orientation: cell.domino.orientation,
          direction: cell.domino.direction,
        ),
        part: cell.part,
      );
      boardUpdate.putIfAbsent(point, () => Cell(point: point));
    }

    return boardUpdate;
  }

  Board _stepFill() {
    Board boardUpdate = {};

    for (var y = -_radius; y <= _radius; y += 2) {
      for (var x = -_radius; x <= _radius; x += 2) {
        Point point = Point(x: x, y: y);
        if (!_board.containsKey(point)) continue;

        if (boardUpdate.containsKey(point)) continue;

        if (point.square.any(
          (p) => !(_board[p].runtimeType == Cell),
        )) continue;

        Orientation orientation = Orientation.values[rnd.nextInt(2)];

        Domino domino = Domino(
          point: point,
          orientation: orientation,
          direction: Direction.negative,
        );
        Domino neighbor = domino.neighbor;

        DominoCell dominoHead = domino.head;
        DominoCell dominoTail = domino.tail;
        DominoCell neighborHead = neighbor.head;
        DominoCell neighborTail = neighbor.tail;

        boardUpdate.addAll({
          dominoHead.point: dominoHead,
          dominoTail.point: dominoTail,
          neighborHead.point: neighborHead,
          neighborTail.point: neighborTail,
        });
      }
    }
  
    return boardUpdate;
  }

  Board _stepAnnihilate() {
    Board boardUpdate = {};

    for (var cellEntry in _board.entries) {
      Point point = cellEntry.key;
      Cell cell = cellEntry.value;

      if (cell.runtimeType == Cell) continue;

      cell = cell as DominoCell;
      Domino domino = cell.domino;

      Point oppositePoint;
      switch (domino.orientation) {
        case Orientation.horizontal:
          switch (domino.direction) {
            case Direction.negative:
              oppositePoint = Point(x: point.x, y: point.y - 2);
              break;
            case Direction.positive:
              oppositePoint = Point(x: point.x, y: point.y + 2);
              break;
          }
          break;
        case Orientation.vertical:
          switch (domino.direction) {
            case Direction.negative:
              oppositePoint = Point(x: point.x - 2, y: point.y);
              break;
            case Direction.positive:
              oppositePoint = Point(x: point.x + 2, y: point.y);
              break;
          }
          break;
      }

      Cell? oppositeCell = _board[oppositePoint];
      if (oppositeCell == null) continue;

      if (oppositeCell.runtimeType == Cell) continue;

      oppositeCell = oppositeCell as DominoCell;
      Domino oppositeDomino = oppositeCell.domino;

      if (oppositeDomino.orientation == domino.orientation &&
          oppositeDomino.direction != domino.direction) {
        boardUpdate[point] = Cell(point: point);
      }
    }

    return boardUpdate;
  }

  void _mutateBoard(Iterable<MapEntry<Point, Cell>> cellEntries) {
    Board nextBoard = Map.from(_board);
    nextBoard.addEntries(cellEntries);
    _board = nextBoard;
  }
}

enum Step { extend, slide, fill, annihilate }

class Point {
  final int x;
  final int y;

  const Point({required this.x, required this.y});

  List<Point> get square => [
        for (var dy = 0; dy <= 2; dy += 2)
          for (var dx = 0; dx <= 2; dx += 2) Point(x: x + dx, y: y + dy)
      ];

  @override
  String toString() {
    return "Point{x: $x, y: $y}";
  }

  @override
  int get hashCode => Object.hash(x, y);

  @override
  bool operator ==(Object other) {
    if (other.runtimeType != Point) return false;
    other = other as Point;
    return x == other.x && y == other.y;
  }
}

enum CellColor { black, white, green, blue, yellow, red }

class Cell {
  final Point point;
  final CellColor color;

  Cell({
    required this.point,
    CellColor? color,
  }) : color = color ??
            (((point.x + point.y) % 4 == 0)
                ? CellColor.white
                : CellColor.black);

  @override
  String toString() {
    return "Cell{point: $point, color: ${color.name}}";
  }
}

class Domino extends Cell {
  final Orientation orientation;
  final Direction direction;

  Domino({
    required super.point,
    required this.orientation,
    required this.direction,
  }) : super(color: () {
          switch (orientation) {
            case Orientation.horizontal:
              switch (direction) {
                case Direction.negative:
                  return CellColor.green;
                case Direction.positive:
                  return CellColor.blue;
              }
            case Orientation.vertical:
              switch (direction) {
                case Direction.negative:
                  return CellColor.yellow;
                case Direction.positive:
                  return CellColor.red;
              }
          }
        }());

  DominoCell get head =>
      DominoCell(point: point, domino: this, part: DominoPart.head);
  DominoCell get tail {
    Point tailPoint;
    switch (orientation) {
      case Orientation.horizontal:
        tailPoint = Point(x: point.x + 2, y: point.y);
        break;
      case Orientation.vertical:
        tailPoint = Point(x: point.x, y: point.y + 2);
        break;
    }
    return DominoCell(point: tailPoint, domino: this, part: DominoPart.tail);
  }

  Domino get neighbor {
    assert(direction == Direction.negative);
    Point neighborPoint;
    switch (orientation) {
      case Orientation.horizontal:
        neighborPoint = Point(x: point.x, y: point.y + 2);
        break;
      case Orientation.vertical:
        neighborPoint = Point(x: point.x + 2, y: point.y);
        break;
    }
    return Domino(
      point: neighborPoint,
      orientation: orientation,
      direction: Direction.positive,
    );
  }
}

class DominoCell extends Cell {
  final Domino domino;
  final DominoPart part;

  DominoCell({
    required super.point,
    required this.domino,
    required this.part,
  }) : super(color: domino.color);
}

enum Orientation { horizontal, vertical }

enum Direction { negative, positive }

enum DominoPart { head, tail }
