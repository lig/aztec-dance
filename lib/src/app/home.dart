import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'package:aztec_dance/src/settings/settings_view.dart';

import '../models.dart';
import './field.dart';
import './info.dart';
import './sidebar.dart';

class HomeView extends StatelessWidget {
  const HomeView({super.key});

  static const routeName = '/home';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Aztec Dance Simulation'),
        actions: [
          IconButton(
            icon: const Icon(Icons.play_circle),
            onPressed: () => context.read<FieldModel>().step(),
          ),
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () =>
                Navigator.of(context).pushNamed(SettingsView.routeName),
          ),
        ],
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(width: 250, child: SideBarView()),
          Expanded(
            child: Column(
              children: const [
                InfoView(),
                Expanded(child: FieldView()),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
