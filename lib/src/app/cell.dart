import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models.dart';

class CellView extends StatelessWidget {
  static const int cellSize = 12;

  final Cell cell;

  const CellView({super.key, required this.cell});

  double get viewX => (cell.point.x - 1) / 2 * cellSize;
  double get viewY => (cell.point.y - 1) / 2 * cellSize;

  @override
  Widget build(BuildContext context) {
    Color cellColor;
    switch (cell.color) {
      case CellColor.black:
        cellColor = Colors.black38;
        break;
      case CellColor.white:
        cellColor = Colors.white38;
        break;
      case CellColor.green:
        cellColor = Colors.green;
        break;
      case CellColor.blue:
        cellColor = Colors.blue;
        break;
      case CellColor.yellow:
        cellColor = Colors.yellow;
        break;
      case CellColor.red:
        cellColor = Colors.red;
        break;
    }

    return Positioned(
      left: viewX,
      bottom: viewY,
      child: Selector<ParamsModel, FieldColor>(
        selector: (context, model) => model.fieldColor,
        builder: (context, fieldColor, child) {
          Color color;
          switch (fieldColor) {
            case FieldColor.checkered:
              color = cellColor;
              break;
            case FieldColor.monochrome:
              color = (cell.runtimeType == Cell) ? Colors.white24 : cellColor;
              break;
          }
          return Tooltip(
            message: "(${cell.point.x}, ${cell.point.y})",
            child: Container(
              width: cellSize.toDouble(),
              height: cellSize.toDouble(),
              decoration: BoxDecoration(
                color: color,
                border: Border.all(
                  color: Colors.grey,
                  strokeAlign: StrokeAlign.center,
                  width: 0.5,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
