import 'package:flutter/material.dart' hide Step;
import 'package:provider/provider.dart';

import '../models.dart';

class InfoView extends StatelessWidget {
  const InfoView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => PhysicalModel(
        color: Theme.of(context).shadowColor,
        elevation: 20.0,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: Theme.of(context).cardColor,
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Selector<FieldModel, int>(
                      selector: (context, field) => field.size,
                      builder: (context, size, child) =>
                          Text("Field size: $size"),
                    ),
                    const VerticalDivider(),
                    Selector<FieldModel, int>(
                      selector: (context, field) => field.board.length,
                      builder: (context, numSquares, child) =>
                          Text("Number of squares: $numSquares"),
                    ),
                  ],
                ),
                Selector<FieldModel, Step>(
                  selector: (context, field) => field.nextStep,
                  builder: (context, nextStep, child) =>
                      Text("Next step: ${nextStep.name}"),
                ),
              ],
            ),
          ),
        ),
      );
}
