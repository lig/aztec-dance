import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import '../models.dart';

class SideBarView extends StatelessWidget {
  const SideBarView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => DecoratedBox(
        decoration: BoxDecoration(
          color: Theme.of(context).drawerTheme.backgroundColor,
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Consumer<ParamsModel>(
            builder: (context, model, child) => ListView(
              children: [
                ListTile(
                  title: const Text('Field color'),
                  subtitle: DropdownButtonFormField(
                    value: model.fieldColor,
                    items: FieldColor.values
                        .map((fieldColor) => DropdownMenuItem(
                              value: fieldColor,
                              child: Text(fieldColor.name),
                            ))
                        .toList(),
                    onChanged: (value) =>
                        context.read<ParamsModel>().fieldColor = value,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
