import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models.dart';
import './cell.dart';

class FieldView extends StatelessWidget {
  const FieldView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => DecoratedBox(
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
        ),
        child: Selector<FieldModel, Board>(
          selector: (context, model) => model.board,
          builder: (context, board, child) => _CenterStack(
            children: board.entries.map((cellEntry) {
              return CellView(
                key: Key(cellEntry.value.toString()),
                cell: cellEntry.value,
              );
            }).toList(),
          ),
        ),
      );
}

class _CenterStack extends StatelessWidget {
  final List<Widget> children;

  const _CenterStack({Key? key, required this.children}) : super(key: key);

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Expanded(
            child: Row(
              children: [
                Expanded(child: Container()),
                Expanded(
                  child: Stack(
                    clipBehavior: Clip.none,
                    children: children,
                  ),
                ),
              ],
            ),
          ),
          Expanded(child: Container()),
        ],
      );
}
